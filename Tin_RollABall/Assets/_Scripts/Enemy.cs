﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    GameObject player; //reference to the player game object
    Rigidbody rb; // reference to my rigidbody
    public float speed; // how fast i move, set in editor

    // Start is called before the first frame update
    void Start()
    {
        // set up references:
        // search for a gameobject tagged "Player to make sure add a tag.. 
        player = GameObject.FindGameObjectWithTag("Player");
        rb = GetComponent<Rigidbody>(); // find my rigidbody and save a reference to it 
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // follow the player:
        // what is the direction between me and the player 
        Vector3 dirToPlayer = player.transform.position - transform.position;
        dirToPlayer = dirToPlayer.normalized;

        rb.AddForce(dirToPlayer * speed); 
       
    }
}
