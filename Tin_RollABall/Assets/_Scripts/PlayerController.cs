﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement; 

public class PlayerController : MonoBehaviour
{
    public float speed;
    public Text countText;
    public Text winText;


    private Rigidbody rb;
    private int count;

    public float timePlayed; // how much time the player has been playing (seconds)
    public bool gameFinished; // whether or not the player has collected everything/not

    void Update()
    {
        if (!gameFinished) // while the game is not over yet 
        {
            timePlayed += Time.deltaTime; // add the amount of time that has passed 
            // since the last frame to the following prints to the time played to the console, 
            print("Time Played:" + timePlayed); 
        }
    }


    void Start ()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";

    }
  

    void FixedUpdate ()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive (false);
            count = count + 1;
            SetCountText ();
        }
    }

    void SetCountText ()
    {
        countText.text = "Count: " + count.ToString ();
        if (count >= 12)
        {
            gameFinished = true; // Record that the player has collected everything
            winText.text = "You Win!";
            Invoke("RestartLevel", 2f);  // Call level restart after 2 seconds
        }
    }

    void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}


